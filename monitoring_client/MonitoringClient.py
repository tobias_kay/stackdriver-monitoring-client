import sys
import time
import importlib
import random
from google.cloud import monitoring_v3
import yaml
import metrics
from metrics import LifeSignProvider


class MonitoringClient:
    def __init__(self, config: dict, debug=False):
        self.config = config
        self.debug = debug

        for attribute in ['project_name', 'resource_type']:
            if attribute not in self.config:
                raise Exception("No attribute '{}' defined in '{}'!".format(attribute, config['__config_file']))

        if 'labels' not in self.config:
            self.config['labels'] = []

        if 'last_updated' not in self.config:
            self.config['last_updated'] = 0

        self.project_name = self.config['project_name']

        self.metrics_client = monitoring_v3.MetricServiceClient()
        self.project_path = self.metrics_client.project_path(self.project_name)

        self.metric_providers = []
        has_life_sign_provider = False
        if 'metrics' not in self.config:
            self.config['metrics'] = []

        for i, metric_cfg in enumerate(self.config['metrics']):
            provider_name = metric_cfg['provider']
            importlib.import_module("metrics.{}".format(provider_name))

            provider = vars(vars(sys.modules["metrics"])[provider_name])[provider_name](metric_cfg)
            provider.config_id = i
            if isinstance(provider, LifeSignProvider.LifeSignProvider):
                has_life_sign_provider = True
            self.metric_providers.append(provider)

        if not has_life_sign_provider and 'life_sign' in self.config and self.config['life_sign']:
            print("Creating life sign metric provider...")
            ls_provider = LifeSignProvider.LifeSignProvider({})
            ls_provider.config_id = len(self.config['metrics'])
            self.config['metrics'].append({
                'provider': 'LifeSignProvider'
            })
            self.metric_providers.append(ls_provider)

        self.register_metrics()

    def deregister_metrics(self):
        for metric_provider in self.metric_providers:
            descriptor = metric_provider.get_descriptor()

            print("deleting metric '{}'...".format("{}/metricDescriptors/{}".format(self.project_path, descriptor.type)))
            self.metrics_client.delete_metric_descriptor(
                "{}/metricDescriptors/{}".format(self.project_path, descriptor.type))

    # def create_life_sign_metric(self):

    def register_metrics(self):
        print("registering metrics...")
        for metric_provider in self.metric_providers:
            descriptor = metric_provider.get_descriptor()
            matches = self.metrics_client.list_metric_descriptors(self.project_path,
                                                                  'metric.type = "{}"'.format(descriptor.type))
            found = False
            for match in matches:
                if match.type == descriptor.type:
                    found = True

            if not found:
                print("Did not find metric '{}'! Attempting to create it...".format(descriptor.type))
                self.metrics_client.create_metric_descriptor(self.project_path, descriptor)
            elif self.debug:
                print("Found metric '{}'!".format(descriptor.type))

    def publish_metrics(self):
        print("publishing metrics...")
        time_series = []
        for metric_provider in self.metric_providers:

            descriptor = metric_provider.get_descriptor()
            now = time.time()
            d_time = now - metric_provider.config['last_updated']

            if d_time < metric_provider.get_interval() or descriptor is None:
                continue

            series = monitoring_v3.types.TimeSeries()
            point = series.points.add()
            if not metric_provider.set_data(point):
                continue
            series.metric.type = descriptor.type
            if 'resource_type' in metric_provider.config:
                series.resource.type = metric_provider.config['resource_type']
            else:
                series.resource.type = self.config['resource_type']
            series.resource.labels.update(self.config['labels'])
            series.resource.labels.update(metric_provider.get_resource_labels())

            point.interval.end_time.seconds = int(now)
            point.interval.end_time.nanos = int((now - point.interval.end_time.seconds) * 10**9)
            time_series.append(series)

            self.config['metrics'][metric_provider.config_id]['last_updated'] = now
        if len(time_series) > 0:
            self.metrics_client.create_time_series(self.project_path, time_series)
        self.config['last_updated'] = time.time()

    def run_once(self):
        self.publish_metrics()

    def save(self):

        config_file = self.config['__config_file']
        del self.config['__config_file']
        with open(config_file, 'w') as handler:
            handler.write(yaml.dump(self.config, default_flow_style=False))

