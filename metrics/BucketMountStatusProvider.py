from google.cloud import monitoring_v3, logging
from metrics.MetricProvider import MetricProvider


class BucketMountStatusProvider(MetricProvider):
    def __init__(self, cfg: dict):
        super(BucketMountStatusProvider, self).__init__(cfg)
        if 'mount_points' not in self.config:
            self.config['mount_points'] = {}
        self._descriptor = None
        self._logging_client = logging.Client()
        self._log_name = "bucket-mount-errors"
        self._logger = self._logging_client.logger(self._log_name)

    def get_descriptor(self) -> monitoring_v3.types.MetricDescriptor:
        if self._descriptor is not None:
            return self._descriptor

        self._descriptor = monitoring_v3.types.MetricDescriptor()
        self._descriptor.metric_kind = monitoring_v3.enums.MetricDescriptor.MetricKind.GAUGE
        self._descriptor.type = 'custom.googleapis.com/bucket-mount-errors'
        self._descriptor.value_type = monitoring_v3.enums.MetricDescriptor.ValueType.INT64
        self._descriptor.description = 'Logs the number of buckets which are not properly mounted'
        return self._descriptor

    def get_resource_type(self) -> str:
        return "generic_task"

    def set_data(self, point) -> bool:

        with open('/proc/mounts', 'r') as mounts_file:
            system_mounts = {line.split(" ")[0]: line.split(" ")[1] for line in mounts_file.readlines()}
            failed = 0
            for bucket, mount_point in self.config['mount_points'].items():
                if bucket not in system_mounts or system_mounts[bucket] != mount_point:
                    self._logger.log_text("Bucket '{}' not mounted to '{}'!".format(bucket, mount_point))
                    failed += 1
            print("Found {} mount errors!".format(failed))
            point.value.int64_value = failed
        return True

    def get_interval(self) -> float:
        return 600.0
