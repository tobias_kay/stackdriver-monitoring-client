from google.cloud import monitoring_v3
from metrics.MetricProvider import MetricProvider


class LifeSignProvider(MetricProvider):

    def __init__(self, cfg: dict):
        super(LifeSignProvider, self).__init__(cfg)
        self._descriptor = None

    def get_descriptor(self) -> monitoring_v3.types.MetricDescriptor:
        if self._descriptor is not None:
            return self._descriptor

        self._descriptor = monitoring_v3.types.MetricDescriptor()
        self._descriptor.metric_kind = monitoring_v3.enums.MetricDescriptor.MetricKind.GAUGE
        self._descriptor.type = 'custom.googleapis.com/monitoring_life_sign'
        self._descriptor.value_type = monitoring_v3.enums.MetricDescriptor.ValueType.BOOL
        self._descriptor.description = 'Whether or not the metric client and it\'s environment are operational'
        return self._descriptor

    def get_resource_type(self) -> str:
        return "generic_task"

    def set_data(self, point) -> bool:
        point.value.bool_value = True
        return True

    def get_interval(self) -> float:
        return 60.0


