from google.cloud import monitoring_v3


class MetricProvider:
    def __init__(self, cfg: dict):
        self.config = cfg
        if 'last_updated' not in self.config:
            self.config['last_updated'] = 0.0
        if 'provider' not in self.config:
            self.config['provider'] = type(self).__name__

    def get_interval(self) -> float:
        raise NotImplementedError()

    def get_descriptor(self) -> monitoring_v3.types.MetricDescriptor:
        raise NotImplementedError()

    def set_data(self, point) -> bool:
        raise NotImplementedError()

    def get_resource_type(self) -> str:
        raise NotImplementedError()

    def get_resource_labels(self) -> dict:
        return {}
