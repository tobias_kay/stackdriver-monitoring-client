
import argparse
import yaml
import os.path as path
import os
from monitoring_client import MonitoringClient


def validate_config_yml(config_path):
    if not path.isfile(config_path):
        raise argparse.ArgumentTypeError("Could not find config file '{}'".format(config_path))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", help="path to the config.yml", type=validate_config_yml)
    parser.add_argument("-p", "--project", help="Project to publish the metrics in")
    args = parser.parse_args()

    config_file = "config.yml"
    if args.config is not None:
        config_file = args.config

    with open(config_file, 'r') as stream:
        config = yaml.load(stream)
    debug = "debug" in config and config["debug"]
    if debug:
        print(config)
    config['__config_file'] = path.abspath(config_file)

    client = MonitoringClient(config, debug)
    client.run_once()
    client.save()
    # client.deregister_metrics()
